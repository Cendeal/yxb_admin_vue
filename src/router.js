import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/login/Login'
import Home from '@/views/admin/Home'
import Tree from '@/views/admin/tree/Tree'
import Member from "./views/admin/member/Member";
import Department from "./views/admin/department/Department";
// import Manager from "./views/admin/manager/Manager";
import Group from "./views/admin/group/Group";
import Welcome from "./views/admin/welcome/Welcome";
Vue.use(VueRouter)
const router = new VueRouter({
    mode:"history",
    routes:[
        {
            path: '/yxb_vue/login',
            component:Login
        },
        {
            path: '/yxb_vue/home',
            component:Home,
            children:[
                {
                    path:'/',
                    component:Welcome
                },
                {
                    path:'tree',
                    component:Tree
                },
                {
                    path:'member',
                    component:Member
                },
                {
                    path:'department',
                    component:Department
                },
                {
                    path:'group',
                    component:Group
                }
            ]
        }
        ]
})
export default router