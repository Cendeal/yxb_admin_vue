import axios from 'axios'
const host = 'http://www.cendeal.cn/yxb/api/v1/admin'
const requests = {
    state:{
        api:{
            login:{
                url:host+'/login',
                method:'post',
                data:null
            },
            group_get:{
                url:host+'/group/get',
                method:'get',
                params:{}
            },
            logout:{
                url:host+'/logout',
                method:'get',
                params:{}
            }
        }
    },
    mutations:{
        beforeLogin:(state,obj)=>{
            let form_data=new FormData()
            form_data.append('user_name', obj.user_name)
            form_data.append('password', obj.password)
            state.api.login.data = form_data
        }
    },
    getters:{

    },
    actions:{
        doLogin({commit,state},obj){
            commit('beforeLogin',obj)
            return axios(state.api.login)
        },
        doLogout({state}){
            return axios(state.api.logout)
        }
    }
}
export default requests