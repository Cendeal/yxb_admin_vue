### 前端

#### 1.json描述公司结构

```
{
    "name":"某公司",
    "departments":[
        {
            "name":"业务部",
            "groups":[
                {"name":"业务一部",
                 "members":[{"name":"张某"},{"name":"王某"}]
                },
                {"name":"业务二部",
                 "members":[{"name":"王某"}]
                }]
        },
         {
            "name":"研发部",
            "groups":[
                {"name":"研发一组",
                 "members":[{"name":"陈某"}]
                },
                {"name":"研发二组",
                 "members":[{"name":"李某"}]
                }]
        }]
}
```



#### 2.页面显示该组织结构

![组织结构](https://gitee.com/Cendeal/yxb_admin_vue/raw/c64376a1cc44d2b43f48923fd68d06e4319962d6/src/demo/company.PNG)

#### 3.实现一个人员管理界面

​	a.       查询条件：姓名，所属部门

​	b.       数据列表：姓名，所属部门 ….

​	c.        基础数据：自行定义

分组

![分组](https://gitee.com/Cendeal/yxb_admin_vue/raw/c64376a1cc44d2b43f48923fd68d06e4319962d6/src/demo/group.PNG)

人员

![member](https://gitee.com/Cendeal/yxb_admin_vue/raw/c64376a1cc44d2b43f48923fd68d06e4319962d6/src/demo/member.PNG)

部门

![部门](https://gitee.com/Cendeal/yxb_admin_vue/raw/c64376a1cc44d2b43f48923fd68d06e4319962d6/src/demo/department.PNG)



#### 4.总结实现方案

前端实现使用的是vue.js，5月6日花了一天的时间完成到目前的情况，使用的是iveiw前端样式，基本页面都设计完成了，由于时间有限，还没有完全对接后端接口，目前已对接的接口有登陆和登出。还有几个页面的只写了基本的主体，细节还没处理。这个iveiw的样式也是当天才接触的使用的。之前使用的是musui样式，layui等，均开发过项目。目前对对vuex、axios、vue-router、vue-jsonp等都比较熟练。

部署地址:http://www.cendeal.cn/yxb_vue/login 账号：cendeal密码：12345678

gitee地址:https://gitee.com/Cendeal/yxb_admin_vue





